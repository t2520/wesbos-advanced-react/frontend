import { useMutation } from '@apollo/client';
import gql from 'graphql-tag';
import Router from 'next/router';
import useForm from '../lib/useForm';
import DisplayError from './ErrorMessage';
import { ALL_PRODUCTS_QUERY } from './Products';
import Form from './styles/Form';

const CREATE_PRODUCT_MUTATION = gql`
  mutation CREATE_PRODUCT_MUTATION(
    # which variables are getting passed in and what types are they. The ! at end means required
    $name: String!
    $description: String!
    $price: Int!
    $image: Upload
  ) {
    createProduct(
      data: {
        name: $name
        description: $description
        price: $price
        status: "AVAILABLE"
        # The photo is a relationship, but also create the image and setup the relationship.
        photo: { create: { image: $image, altText: $name } }
      }
    ) {
      # Output below data back from mutation.
      id
      price
      description
      name
    }
  }
`;

export default function CreateProduct() {
  const { inputs, handleChange, clearForm } = useForm({
    image: '',
    name: 'Nice Shoes',
    price: 34234,
    description: 'These are the best shoes!',
  });

  // //   Method of using variables instead of destructuring. The useMutation requires and argument.
  //   const anArray = useMutation(CREATE_PRODUCT_MUTATION);
  //   const loadingVar = anArray[1].loading;
  //   console.log(`loadingVar is ${loadingVar}`);

  const [createProduct, { loading, error, data }] = useMutation(
    CREATE_PRODUCT_MUTATION,
    {
      //   because the inputs in the object are names as expected by the mutation
      //   can just pass on the whole object.  useMutation returns [createProduct, {loading, error, data}]
      variables: inputs,
      //   When mutation is sent the data is stored in the database, but if the
      //   products page is already stored in the cache, it may not show up when
      //   products page is navigated to.  For this reason the new product can
      //   be pushed directly to the cache or the query can be send to the
      //   server again.  Where it is pushed to the cache it is called an
      //   optimistic update, that assumes this has been saved on the server. In
      //   This case we will use refetch to get the updated data from the
      //   database server.
      refetchQueries: [{ query: ALL_PRODUCTS_QUERY }],
    }
  );

  return (
    <Form
      onSubmit={async (event) => {
        event.preventDefault();
        console.log(inputs);
        // Submit the input fields to the backend.  Variables could have also
        // been passed in here if they had not been known in advance while
        // creating the mutation.
        const response = await createProduct({
          //   // variables could have been passed here
          //   variables: inputs,
        });
        console.log(response);
        clearForm();
        // Go to product page.
        Router.push({
          pathname: `/product/${response.data.createProduct.id}`,
        });
      }}
    >
      <DisplayError error={error} />
      {/* Fieldset groups together multiple inputs and allows us to disable multiple 
      inputs while the form data is submitted to the back end  The aria links if for
      accessibility and the CSS animation uses it, the disabled property disabled all
      the enclosed inputs while awaiting the create product   Disabled and error props
      come from the useMutation hook. */}
      <fieldset disabled={loading} aria-busy={loading}>
        <label htmlFor="image">
          Image
          <input
            required
            type="file"
            id="image"
            name="image"
            onChange={handleChange}
          />
        </label>
        <label htmlFor="name">
          Name
          <input
            type="text"
            id="name"
            name="name"
            placeholder="Name"
            value={inputs.name}
            onChange={handleChange}
          />
        </label>
        <label htmlFor="price">
          Price
          <input
            type="number"
            id="price"
            name="price"
            placeholder="Price"
            value={inputs.price}
            onChange={handleChange}
          />
        </label>
        <label htmlFor="description">
          Description
          <textarea
            id="description"
            name="description"
            placeholder="Description"
            value={inputs.description}
            onChange={handleChange}
          />
        </label>
        <button type="submit">+ Add Product</button>
      </fieldset>
    </Form>
  );
}
