import PropTypes from 'prop-types';
import styled, { createGlobalStyle } from 'styled-components';
import Header from './Header';

// Global style to inject into all other pages as which includes a browser
// reset. Can also import normalize.css here
const GlobalStyles = createGlobalStyle`
/* Setup variables, putting then in a :root{} means you an reuse that CSS inside
a stand alone SVG element */
@font-face {
  font-family: 'radnika_next';
  src: url('/static/radnikanext-medium-webfont.woff2') format('woff2');
  font-weight: normal;
  font-style: normal;
}
html {
  --red: #ff0000;
  --black: #393939;
  --grey: #3A3A3A;
  /* Below means if american spelling is used then look at the --grey var above
  */
  --gray: var(--grey);
  --lightGrey: #e1e1e1;
  --lightGray: var(--lightGrey);
  --offWhite: #ededed;
  --maxWidth: 1000px;
  /* Box Shadow variable to use throughout project */
  --bs: 0 12px 24px 0 rgba(0,0,0,0.09);
  /* Browser reset */
  box-sizing: border-box;
  font-size: 62.5%
}
/* Browser reset */
*, *:before, *:after {
   box-sizing: inherit;
 }

body {
  font-family: 'radnika_next', --apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
/* Browser reset */
padding: 0;
margin: 0;
font-size: 1.5rem;
line-height:2;
}
/* More browser reset */
a {
  text-decoration: none;
  color: var(--black);
}
a:hover {
  text-decoration: underline;
}
/* Buttons needs the font family applies as well as does not inherit from body
with styles components button */
button {
  font-family: 'radnika_next', --apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue',
}
`;
const InnerStyles = styled.div`
  max-width: var(--maxWidth);
  margin: 0 auto;
  padding: 2rem;
`;

export default function Page({ children, cool }) {
  return (
    <div>
      {/* Inject the Global Styles setup above for use in whole project. */}
      <GlobalStyles />
      <Header />
      <InnerStyles>{children}</InnerStyles>
    </div>
  );
}

Page.propTypes = {
  cool: PropTypes.string,
  children: PropTypes.any,
  // // Below did not work but is an idea
  // children: PropTypes.oneOf([
  //   PropTypes.arrayOf(PropTypes.node),
  //   PropTypes.node,
  // ]),
};
