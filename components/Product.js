import Link from 'next/link';
import formatMoney from '../lib/formatMoney';
import ItemStyles from './styles/ItemStyles';
import PriceTag from './styles/PriceTag';
import Title from './styles/Title';

export default function Product({ product }) {
  return (
    <ItemStyles>
      {/* The ? are JavaScript Optional Chaining and check to see if the object
      is valid, if invalid it returns undefined instead of nullish, as nullish
      would and unhandled runtime error */}
      {/* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Optional_chaining */}
      {/* Untested but could use to set a default value with
      image?.publicUrlTransformed ?? "Location to default" */}
      <img
        src={product?.photo?.image?.publicUrlTransformed}
        alt={product.name}
      />
      <Title>
        <Link href={`/product/${product.id}`}>{product.name}</Link>
      </Title>
      <PriceTag>{formatMoney(product.price)}</PriceTag>
      <p>{product.description}</p>
      {/* TODO: Add buttons to edit and delete item. */}
    </ItemStyles>
  );
}
