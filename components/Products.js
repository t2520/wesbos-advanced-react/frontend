import { useQuery } from '@apollo/client';
import gql from 'graphql-tag';
import styled from 'styled-components';
import Product from './Product';
// Turns the basic text from the query into a graphql query. Export the query
// for use in refetching after adding or modifying with a mutation.
export const ALL_PRODUCTS_QUERY = gql`
  query ALL_PRODUCTS_QUERY {
    allProducts {
      id
      name
      price
      description
      photo {
        id
        image {
          publicUrlTransformed
        }
      }
    }
  }
`;

const ProductListStyles = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 60px;
`;

export default function Products() {
  // Use a hook to fetch the data
  const { data, error, loading } = useQuery(ALL_PRODUCTS_QUERY);
  console.log(data, error, loading);
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Loading: {error.message}</p>;
  return (
    <div>
      <ProductListStyles>
        {data.allProducts.map((product) => (
          // Passing the product from the query to the product component as a
          // prop. Prop is also called product hence all the products words
          // below.
          <Product key={product.id} product={product} />
        ))}
      </ProductListStyles>
    </div>
  );
}
