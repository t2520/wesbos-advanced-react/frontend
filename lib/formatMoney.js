// formatMoney starts in lower case as it is not a class.
export default function formatMoney(amount = 0) {
  const formatter = Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    //   Check if whole value but checking for remainder when divided by 100 in
    //   which case don't show cent / pennies.  % outputs a remainder from the
    //   division.
    minimumFractionDigits: amount % 100 === 0 ? 0 : 2,
  });

  return formatter.format(amount / 100);
}
