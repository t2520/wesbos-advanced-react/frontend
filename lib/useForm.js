import { useState } from 'react';

export default function useForm(initialState = {}) {
  // state object for our inputs
  const [inputs, setInputs] = useState(initialState);

  //   Can't just use event.target.value because state is going to be an
  //   object with multiple pieces of state like below, need to setState an
  //   object, copy the existing state... and update only the item that need
  //   to change.  Use the name property of the object so this hook can be
  //   used anywhere.
  // {
  // name: 'Min',
  // description: 'Nice Shoes',
  // price: '1000'
  // }

  // My code that did not work - Fixed by assigning value to event.target.value first.
  function handleChange(event) {
    console.log(event);
    let value;
    value = event.target.value;
    if (event.target.type === 'number') {
      value = parseInt(event.target.value);
    }
    // File uploads use and array of files. We want the 1st item in the array so
    // putting the value in [] achieves this.  E.G is there were more items in
    // the array then they would be pulled our into variable likes this [1st, 2nd, 3rd]
    if (event.target.type === 'file') {
      [value] = event.target.files;
      console.log('---------VALUE of FILES ARRAY--------');
      console.log(value);
    }
    setInputs({
      // Copy the existing state
      ...inputs,
      // Update (overwrite) only the piece of state with name matching the form field
      [event.target.name]: value,
    });
  }

  // // WES solution
  // function handleChange(e) {
  //   let { value, name, type } = e.target;
  //   if (type === 'number') {
  //     value = parseInt(value);
  //   }
  //   if (type === 'file') {
  //     [value] = e.target.files;
  //   }
  //   setInputs({
  //     // copy the existing state
  //     ...inputs,
  //     [name]: value,
  //   });
  // }

  // Reset form back to initial state
  function resetForm() {
    setInputs(initialState);
  }

  function clearForm() {
    const blankState = Object.fromEntries(
      Object.entries(inputs).map(([key, value]) => [key, ''])
    );
    setInputs(blankState);
  }

  // Return the things we want to surface (so the input parameters) from this custom hook
  return {
    inputs,
    handleChange,
    resetForm,
    clearForm,
  };
}
