import { ApolloClient, ApolloLink, InMemoryCache } from '@apollo/client';
import { onError } from '@apollo/link-error';
import { getDataFromTree } from '@apollo/client/react/ssr';
import { createUploadLink } from 'apollo-upload-client';
import withApollo from 'next-with-apollo';
import { endpoint, prodEndpoint } from '../config';

// Create client and inject some links for error handling
function createClient({ headers, initialState }) {
  return new ApolloClient({
    link: ApolloLink.from([
      onError(({ graphQLErrors, networkError }) => {
        if (graphQLErrors)
          graphQLErrors.forEach(({ message, locations, path }) =>
            console.log(
              `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
            )
          );
        if (networkError)
          console.log(
            `[Network error]: ${networkError}. Backend is unreachable. Is it running?`
          );
      }),
      // this uses apollo-link-http under the hood, so all the options here come from that package
      createUploadLink({
        uri: process.env.NODE_ENV === 'development' ? endpoint : prodEndpoint,
        // Include cookies when sending request for data from GraphQl End point.
        // By sending along all the cookies and header with every request the
        // server can server side render all the logged in states so there is no
        // initial blip on the site showing logged in until the clint re-renders
        // it.
        fetchOptions: {
          credentials: 'include',
        },
        // pass the headers along from this request. This enables SSR with logged in state
        headers,
      }),
    ]),
    // Store the cache in memory (browser) as makes going back and forth through
    // the pages quicker. It is cleared on browser refresh.
    cache: new InMemoryCache({
      typePolicies: {
        Query: {
          fields: {
            // TODO: We will add this together!
            // allProducts: paginationField(),
          },
        },
      },
      // Because views are initially rendered views on the server, need to pass
      // the data to the client.
    }).restore(initialState || {}),
  });
}

// withApollo and getDataFromTree are a packages that reads any queries for data
// on the server, fetch this data and use it to render before sending to client.
export default withApollo(createClient, { getDataFromTree });
