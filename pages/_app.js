// In next JS must have a pages directory and also must have a _app.js file to
// control things higher than the page component like the body and header.

import { ApolloProvider } from '@apollo/client';
import NProgress from 'nprogress';
import Router from 'next/router';
import Page from '../components/Page';

import '../components/styles/nprogress.css';
import withData from '../lib/withData';

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

function MyApp({ Component, pageProps, apollo }) {
  // console.log(apollo);
  // This can be instead of inserting a page component into every other
  // component to wrap the rest of output.
  return (
    // The provider allows components to access all data like price etc,
    // Exported application is wrapped in withData which provides the whole
    // application component including the data.
    <ApolloProvider client={apollo}>
      <Page>
        <Component {...pageProps} />
      </Page>
    </ApolloProvider>
  );
}

// Tell Next js to fetch all queries on all children components. Ctx = context,
// using word context breaks it.
MyApp.getInitialProps = async function ({ Component, ctx }) {
  let pageProps = {};
  if (Component.getInitialProps) {
    // If any of the pages have a getInitialProps then wait and fetch this data.
    pageProps = await Component.getInitialProps(ctx);
  }
  // Get query variable like /2 or /products
  pageProps.query = ctx.query;
  return { pageProps };
};

export default withData(MyApp);
