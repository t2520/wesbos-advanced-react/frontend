import Products from '../components/Products';

export default function ProductsPage() {
  return (
    <div>
      <p>Products component</p>
      <Products />
    </div>
  );
}
